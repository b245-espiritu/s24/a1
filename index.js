// 
let getCube = function(num){
   console.log(`The cube of ${num} is : ${Math.pow(num, 3)}`);
}

getCube(2);

let address = ['443', 'brgy. Baloktot', 'Curdova'];

let [houseNo, brgy, municipality, ] = address;
console.log( `I live at ${houseNo}, ${brgy}, ${municipality}`);

const animal = {
    animalName: 'Lolong',
    animalType:'saltwater crocodile',
    animalWeigth: 1075,
    animalSize: '20 ft 3 in'
}


const {animalName, animalType, animalWeigth, animalSize} = animal;

console.log(`${animalName} was a ${animalType}. He weighed at ${animalWeigth}kgs with a measurement of ${animalSize}`);



const arrayNumbers = [ 1,2,3,4,5];
arrayNumbers.forEach(item=> console.log(item));

const reduceNumber = arrayNumbers.reduce((a,b)=> a + b);
console.log(reduceNumber);



class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const balbonyDog = new Dog('balbony', 3, 'golden retriever');

console.log(balbonyDog);